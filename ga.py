import Levenshtein
import random
import string

population_size = 1000             # Number of individuals in each generation
mutation_rate = 0.01               # Chance that there will be a mutation at a given index in a candidate string
crossover = True                   # Should offspring be based on two parents (True) or one parent (False)
retained_population_percent = 1.0  # What percentage of each generation should get to reproduce?


def fitness(candidate):
    target_string = 'Hello SPT-UK Tech Talks December 2016 Session!'
    return Levenshtein.ratio(candidate, target_string)


def random_char():
    return random.SystemRandom().choice(string.ascii_letters + string.digits + string.punctuation + ' ')


def random_individual():
    return ''.join(random_char() for _ in
                   range(random.SystemRandom().randint(10, 100)))


def random_population():
    result = []
    for _ in range(population_size):
        result.append(random_individual())
    return result


def reproduce(first_parent, second_parent):
    result = []
    for i in range(max(len(first_parent), len(second_parent))):
        if i >= len(first_parent):
            result.append(second_parent[i])
        elif i >= len(second_parent):
            result.append(first_parent[i])
        else:
            result.append(random.choice([first_parent[i], second_parent[i]]))
    return ''.join(result)


def mutation(char):
    return random.choice([random_char() + char, random_char(), char + random_char(), ''])


def mutate(individual):
    return ''.join(map(lambda char: mutation(char) if random.random() < mutation_rate else char, individual))


def next_generation(population):
    result = []
    sorted_population = sorted(population, key=fitness, reverse=True)
    for _ in range(population_size):
        first_parent = random.choice(sorted_population[:int(len(sorted_population) * (retained_population_percent / 100))])
        if crossover:
            second_parent = random.choice(sorted_population[:int(len(sorted_population) * (retained_population_percent / 100))])
            result.append(mutate(reproduce(first_parent, second_parent)))
        else:
            result.append(mutate(first_parent))
    return result


def main():
    gen = 0
    population = random_population()

    while fitness(population[0]) < 1:
        gen += 1
        population = next_generation(population)
        print 'Generation ' + str(gen) + ' best: "' + population[0] + '" with fitness ' + str(fitness(population[0]))
    print 'Found it! Took ' + str(gen) + ' generations.'


if __name__ == "__main__":
    main()
